# enforce tls12 usage
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12

$workdir = "C:\ProgramData\Ngrok-WinACME"
New-Item -Path $workdir -ItemType Directory -Force | Out-Null


# this function handles the installation of ngrok
function global:Install-Ngrok {

    param (

        [Parameter(Mandatory = $False)]
        [switch] $Force

    )

    # this uri has a special string after /c/, hopelly it shouldn't change once a new version is uploaded; we need to verify this manually
    $uri = "https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-windows-amd64.zip"
    $workdir = "C:\ProgramData\Ngrok-WinACME"

    # if ngrok.exe is already running then abort
    if ((Get-WmiObject Win32_Process -Filter "name = 'ngrok.exe'").ExecutablePath -contains "$workdir\ngrok\ngrok.exe") {
        Write-Host -ForegroundColor Yellow -Object "This operation can't be performed while ngrok tunnels are active."
        return
    }

    # create ngrok directories
    New-Item -Path $workdir -Name "ngrok" -ItemType Directory -Force | Out-Null
    New-Item -Path $workdir -Name "ngrok\config" -ItemType Directory -Force | Out-Null
    New-Item -Path $workdir -Name "ngrok\log" -ItemType Directory -Force | Out-Null

    # if ngrok is already there then print version and return
    if (Test-Path -Path "$workdir\ngrok\ngrok.exe" -PathType Leaf) {
        Write-Host -Object "Ngrok binary is already installed!"
        $currentver = (&$workdir\ngrok\ngrok.exe version).Replace("ngrok version ", "")
        Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"

        # if force switch is used then overwrite the existing install
        if ($($Force)) {
            try {
                Write-Host -Object "Forcing reinstall/upgrade..."
                Invoke-WebRequest -Uri $uri -Method Get -OutFile "$workdir\ngrok.zip"
                Write-Host -ForegroundColor Green -Object "Download succeeded."
            }
            catch {
                Write-Warning -Message "Download failed!"
                return
            }
            Write-Host -Object "Installing..."
            Expand-Archive -Path "$workdir\ngrok.zip" -DestinationPath "$workdir\ngrok\" -Force
            $currentver = (&$workdir\ngrok\ngrok.exe version).Replace("ngrok version ", "")
            Write-Host -Object "Removing the downloaded installer..."
            Remove-Item -Path "$workdir\ngrok.zip" -Force
            Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
        }
        else {
            return
        }
    }

    # install the ngrok binary if existing install is not found
    else {
        try {
            Write-Host -Object "Downloading..."
            Invoke-WebRequest -Uri $uri -Method Get -OutFile "$workdir\ngrok.zip"
            Write-Host -ForegroundColor Green -Object "Download succeeded."
        }
        catch {
            Write-Warning -Message "Download failed!"
            return
        }
        Write-Host -Object "Installing..."
        Expand-Archive -Path "$workdir\ngrok.zip" -DestinationPath "$workdir\ngrok\" -Force
        $currentver = (&$workdir\ngrok\ngrok.exe version).Replace("ngrok version ", "")
        Write-Host -Object "Removing the downloaded installer..."
        Remove-Item -Path "$workdir\ngrok.zip" -Force
        Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
    }

}


function global:Ngrok-AuthToken {

    param (

        [Parameter(Mandatory = $True)]
        [string] $AuthToken,

        [Parameter(Mandatory = $True)]
        [string] $ConfigFile

    )

    $workdir = "C:\ProgramData\Ngrok-WinACME"

    if (!(Test-Path -Path "$workdir\ngrok\config\$ConfigFile" -PathType Leaf)) {
        Write-Warning -Message "Config file $ConfigFile for ngrok not found."
        return
    }

    # set ngrok authtoken in config file
    Write-Host -Object "Updating authtoken in ngrok config file $ConfigFile..."
    $config = Get-Content -Path "$workdir\ngrok\config\$ConfigFile" -Raw
    $config = $config -replace "authtoken: .*", "authtoken: $AuthToken"
    $config | Set-Content -Path "$workdir\ngrok\config\$ConfigFile" -NoNewline -Force

}


function global:Ngrok-InstallService {

    param (

        [Parameter(Mandatory = $True)]
        [string] $ConfigFile

    )

    $workdir = "C:\ProgramData\Ngrok-WinACME"
    $ConfigName = $ConfigFile.Replace(".yml", "")
    $key = "HKLM\System\CurrentControlSet\Services"
    $scriptspath = "$home\Desktop\ngrok-winacme"

    # verify dependencies
    if (!(Test-Path -Path "$scriptspath\$ConfigFile" -PathType Leaf)) {
        Write-Warning -Message "Config file $ConfigFile for ngrok not found in repository."
        return
    }

    if (Test-Path -Path "$workdir\ngrok\config\$ConfigFile" -PathType Leaf) {
        Write-Warning -Message "Config file $ConfigFile already exists."
    }

    if (!(Test-Path -Path "$workdir\ngrok\ngrok.exe" -PathType Leaf)) {
        Write-Warning -Message "ngrok installation not found."
        return
    }
    if (!(Test-Path -Path "$workdir\nssm\win64\nssm.exe" -PathType Leaf)) {
        Write-Warning -Message "NSSM installation not found."
        return
    }

    if (Get-Service -Name "ngrok-$ConfigName" -ErrorAction SilentlyContinue) {
        Write-Warning -Message "Service already installed."
        return
    }

    Copy-Item -Path "$scriptspath\$ConfigFile" -Destination "$workdir\ngrok\config" -Force | Out-Null

    # install service
    &$workdir\nssm\win64\nssm.exe install "ngrok-$ConfigName" "$workdir\ngrok\ngrok.exe"

    # set service parameters
    reg add "$key\ngrok-$ConfigName" /v "Type" /t "REG_DWORD" /d "0x10" /f
    reg add "$key\ngrok-$ConfigName" /v "Start" /t "REG_DWORD" /d "0x2" /f
    reg add "$key\ngrok-$ConfigName" /v "DependOnService" /t "REG_MULTI_SZ" /d "Tcpip" /f
    reg add "$key\ngrok-$ConfigName" /v "Description" /t "REG_SZ" /d "This service starts ngrok tunnels as defined in config file." /f
    reg add "$key\ngrok-$ConfigName" /v "DelayedAutostart" /t "REG_DWORD" /d "0x1" /f
    reg add "$key\ngrok-$ConfigName\Parameters" /v "AppParameters" /t "REG_EXPAND_SZ" /d "start --config $workdir\ngrok\config\$ConfigFile --all" /f
    reg add "$key\ngrok-$ConfigName\Parameters" /v "AppPriority" /t "REG_DWORD" /d "0x80" /f
    reg add "$key\ngrok-$ConfigName\Parameters" /v "AppNoConsole" /t "REG_DWORD" /d "0x1" /f

    # set service to delayed automatic
    &$("$ENV:WINDIR\system32\sc.exe") config ngrok-$ConfigName start=delayed-auto

}


function global:Ngrok-RemoveService {

    param (

        [Parameter(Mandatory = $True)]
        [string] $ConfigFile

    )

    $workdir = "C:\ProgramData\Ngrok-WinACME"
    $ConfigName = $ConfigFile.Replace(".yml", "")

    if (Get-Service -Name "ngrok-$ConfigName" -ErrorAction SilentlyContinue) {
        Stop-Service -Name "ngrok-$ConfigName" -Force -Verbose
        &$workdir\nssm\win64\nssm.exe remove "ngrok-$ConfigName" confirm
    }
    else {
        Write-Warning -Message "Service not installed."
    }

    # remove config yml file
    if (Test-Path -Path "$workdir\ngrok\config\$ConfigFile" -PathType Leaf) {
        Remove-Item -Path "$workdir\ngrok\config\$ConfigFile" -Force
    }

}


# this function handles the installation of win-acme client
function global:Install-WinACME {

    param (

        [Parameter(Mandatory = $False)]
        [switch] $Update

    )

    # config variables
    $uri = "https://github.com/win-acme/win-acme/releases/latest/"
    $workdir = "C:\ProgramData\Ngrok-WinACME"

    # determine the latest release
    $web1 = Invoke-WebRequest -Uri $uri -MaximumRedirection 0 -Method Get -UseBasicParsing -ErrorAction SilentlyContinue
    $web2 = Invoke-WebRequest -Uri $uri -MaximumRedirection 1 -Method Get -UseBasicParsing -ErrorAction SilentlyContinue
    $ver1 = (($web1.Links.href).Split('/') | Select-Object -Last 1).Replace("v", "")
    $ver2 = $($web2.Links.href | Select-String -SimpleMatch -Pattern "x64.pluggable.zip").ToString().Split('.') | Select-Object -Last 1 -Skip 3
    if ($ver1.Split('.').Contains($ver2)) {
        $latestver = $ver1
    }
    else {
        $latestver = $ver1 + "." + $ver2
    }

    # determine the link to download
    $web = Invoke-WebRequest -Uri $uri -MaximumRedirection 1 -Method Get -UseBasicParsing -ErrorAction SilentlyContinue
    $link = "https://github.com" + $($web.Links.href | Select-String -SimpleMatch -Pattern "x64.pluggable.zip")

    if ($Update) {
        if (Test-Path -Path "$workdir\win-acme\wacs.exe" -PathType Leaf) {
        }
        else {
            Write-Host -Object "Win-ACME installation not found."
            return
        }
        Write-Host -Object "Checking installed and latest available version..."
        $currentver = (&$workdir\win-acme\wacs.exe --version | Select-String -SimpleMatch -Pattern "Software version").ToString().Split(' ') | Select-Object -First 1 -Skip 3
        Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
        Write-Host -ForegroundColor Yellow -Object "Available version: $latestver"
        if ($currentver -eq $latestver) {
            Write-Host -Object "Latest version already installed."
        }
        else {
            Write-Host -Object "Downloading the latest installer..."
            try {
                Invoke-WebRequest -Uri $link -Method Get -OutFile "$workdir\win-acme.zip"
            }
            catch {
                Write-Warning -Message "Download failed!"
                return
            }
            Write-Host -Object "Removing the existing installation..."
            Remove-Item -Path "$workdir\win-acme" -Recurse -Force
            Write-Host -Object "Installing the downloaded version..."
            New-Item -Path $workdir -Name "win-acme" -ItemType Directory -Force | Out-Null
            Expand-Archive -Path "$workdir\win-acme.zip" -DestinationPath "$workdir\win-acme" -Force
            $currentver = (&$workdir\win-acme\wacs.exe --version | Select-String -SimpleMatch -Pattern "Software version").ToString().Split(' ') | Select-Object -First 1 -Skip 3
            Write-Host -Object "Removing the downloaded installer..."
            Remove-Item -Path "$workdir\win-acme.zip" -Force
            Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
        }
        return
    }

    if (Test-Path -Path "$workdir\win-acme\wacs.exe" -PathType Leaf) {
        Write-Host -Object "Win-ACME already installed, checking installed and latest available version..."
        $currentver = (&$workdir\win-acme\wacs.exe --version | Select-String -SimpleMatch -Pattern "Software version").ToString().Split(' ') | Select-Object -First 1 -Skip 3
        Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
        Write-Host -ForegroundColor Yellow -Object "Available version: $latestver"
    }
    else {
        Write-Host -Object "Win-ACME installation not found!"
        Write-Host -Object "Downloading the installer..."
        try {
            Invoke-WebRequest -Uri $link -Method Get -OutFile "$workdir\win-acme.zip"
        }
        catch {
            Write-Warning -Message "Download failed!"
            return
        }
        Write-Host -Object "Installing..."
        New-Item -Path $workdir -Name "win-acme" -ItemType Directory -Force | Out-Null
        Expand-Archive -Path "$workdir\win-acme.zip" -DestinationPath "$workdir\win-acme" -Force
        $currentver = (&$workdir\win-acme\wacs.exe --version | Select-String -SimpleMatch -Pattern "Software version").ToString().Split(' ') | Select-Object -First 1 -Skip 3
        Write-Host -Object "Removing the downloaded installer..."
        Remove-Item -Path "$workdir\win-acme.zip" -Force
        Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
    }

}


function global:WinACME-NewWinRMCert {

    # config variables
    $workdir = "C:\ProgramData\Ngrok-WinACME"
    $rootpath = "C:\inetpub\wwwroot"
    $adminemail = "abc@xyz.com"

    # iis config variables
    $defaultsite = "Default Web Site"

    # check if iis cmdlets are available
    if (!(Get-Command "*iissite")) {
        throw "IIS cmdlets not found. IIS feature is probably not installed. Please install the IIS feature."
    }

    # check if default iis website is available
    if (!(Get-IISSite -Name $defaultsite)) {
        throw "IIS $defaultsite doesn't exist. Please make sure that IIS is installed throughly and `"$defaultsite`" is available."
    }

    # start the w3 service and default iis website
    Start-Service -Name "W3SVC" | Out-Null
    Write-Host -Object "Waiting for service `"World Wide Web Publishing Service`" to start..."
    while ((Get-Service -Name "W3SVC").Status -ne "Running") {
        Start-Sleep -Seconds 2
    }
    Start-IISSite -Name $defaultsite | Out-Null
    Write-Host -Object "Waiting for IIS $defaultsite to start..."
    while ((Get-IISSite -Name $defaultsite).State -ne "Started") {
        Start-Sleep -Seconds 2
    }

    # get iis hostname
    $iishostname = (Get-IISSiteBinding -Name $defaultsite -Protocol http).bindingInformation.Split(':')[-1]

    # verify iis connectivity test
    $webrequest = Invoke-WebRequest -Uri "http://$iishostname" -UseBasicParsing -MaximumRedirection 0 -Method Head
    if ($webrequest.StatusDescription -ne "OK") {
        throw "IIS connectivity test failed. Make sure that the $iishostname is reachable."
    }

    <# cached certificates can be found here C:\ProgramData\win-acme\acme-v02.api.letsencrypt.org\Certificates\ #>
    Write-Host -Object "Calling win-acme client to order/renew certificates..."
    &$workdir\win-acme\wacs.exe `
        --source manual --host $iishostname `
        --validationmode "http-01" --validation filesystem --webroot $rootpath --manualtargetisiis `
        --order host `
        --csr rsa `
        --store certificatestore --certificatestore "Personal" `
        --installation none `
        --setuptaskscheduler `
        --accepttos `
        --emailaddress $adminemail `
        --verbose

    # stop the default iis website and w3 service
    Stop-IISSite -Name $defaultsite -Confirm:$False | Out-Null
    Stop-Service -Name "W3SVC" -Force | Out-Null
    Set-Service -Name "W3SVC" -StartupType Manual | Out-Null

}


function global:WinACME-NewNgrokCert {

    param (

        [Parameter(Mandatory = $True)]
        [string] $ContName,

        [Parameter(Mandatory = $True)]
        [string] $HostName

    )

    # config variables and services listing
    $workdir = "C:\ProgramData\Ngrok-WinACME"
    $scriptspath = "$home\Desktop\ngrok-winacme"
    $rootpath = "C:\inetpub\wwwroot\http"
    $key = "HKLM\System\CurrentControlSet\Services"
    $services = Get-Service -Name "ngrok-*"
    $adminemail = "abc@xyz.com"

    if (!((Get-Bccontainers -includeLabels).name -contains "$ContName")) {
        Write-Warning -Message "Container $ContName doesn't exist!"
        return
    }

    # install dependencies
    if (!(Test-Path -Path "$workdir\win-acme\wacs.exe" -PathType Leaf)) {
        Install-WinACME
    }

    if (!(Test-Path -Path "$workdir\ngrok\ngrok.exe" -PathType Leaf)) {
        Install-Ngrok
    }

    if (!(Test-Path -Path "$workdir\nssm\win64\nssm.exe" -PathType Leaf)) {
        Install-NSSM
    }

    # copy ngrok acme.yml
    if (!(Test-Path -Path "$workdir\ngrok\config\acme.yml" -PathType Leaf)) {
        Write-Host -Object "Config file acme.yml not found in ngrok config directory, copying the config file from SCRIPTS..."
        Copy-Item -Path "$scriptspath\acme.yml" -Destination "$workdir\ngrok\config" -Force | Out-Null
        Write-Warning -Message "Run this function again after updating the ngrok authtoken for acme.yml file."
        return
    }

    # install ngrok acme service
    if ($services.Name -notcontains "ngrok-acme") {

        # install service
        Write-Host -Object "Creating ngrok-acme service to solve ACME challenges..."
        &$workdir\nssm\win64\nssm.exe install "ngrok-acme" "$workdir\ngrok\ngrok.exe"

        # set service parameters
        reg add "$key\ngrok-acme" /v "Type" /t "REG_DWORD" /d "0x10" /f
        reg add "$key\ngrok-acme" /v "Start" /t "REG_DWORD" /d "0x2" /f
        reg add "$key\ngrok-acme" /v "DependOnService" /t "REG_MULTI_SZ" /d "Tcpip" /f
        reg add "$key\ngrok-acme" /v "Description" /t "REG_SZ" /d "This service starts ngrok tunnel to solve ACME challenge." /f
        reg add "$key\ngrok-acme" /v "DelayedAutostart" /t "REG_DWORD" /d "0x1" /f
        reg add "$key\ngrok-acme\Parameters" /v "AppParameters" /t "REG_EXPAND_SZ" /d "start --config $workdir\ngrok\config\acme.yml --all" /f
        reg add "$key\ngrok-acme\Parameters" /v "AppPriority" /t "REG_DWORD" /d "0x80" /f
        reg add "$key\ngrok-acme\Parameters" /v "AppNoConsole" /t "REG_DWORD" /d "0x1" /f

        # set service startup to manual
        Set-Service -Name "ngrok-acme" -StartupType Manual

    }
    else {
        Write-Host -Object "Service ngrok-acme already exists..."
    }

    # stop all running ngrok services
    Write-Host -Object "Stopping ngrok services..."
    if (!($null -eq $services)) {
        $services | ForEach-Object {
            $service = $_
            if ((Get-Service -Name $service.Name).Status -eq "Running") {
                Stop-Service -Name $service.Name -Force -Verbose
            }
        }
    }

    # create directory to store certs
    New-Item -Path "$workdir" -Name "acme-certs" -ItemType Directory -Force | Out-Null
    New-Item -Path "$workdir\acme-certs" -Name $HostName -ItemType Directory -Force | Out-Null

    # modify acme.yml
    Write-Host -Object "Modifying acme.yml..."
    $acmeyml = Get-Content -Path "$workdir\ngrok\config\acme.yml"
    $acmeyml = $acmeyml -replace " addr: .*$", " addr: http://$($ContName):8080"
    $acmeyml = $acmeyml -replace "hostname: .*$", "hostname: $($HostName)"
    Set-Content -Value $acmeyml -Path "$workdir\ngrok\config\acme.yml" -Force

    # start ngrok tunnel to solve acme challenge
    Write-Host -Object "Starting ngrok-acme service..."
    Start-Service -Name "ngrok-acme"

    # run connectivity test before the calling the win-acme
    $webrequest = Invoke-WebRequest -Uri "http://$HostName" -UseBasicParsing -MaximumRedirection 0 -Method Head
    if ($webrequest.StatusDescription -ne "OK") {
        Write-Warning -Message "Tunnel connectivity test failed. Please check ngrok config file and network connectivity."
        return
    }

    # verify if certs already exist
    if (Test-path -Path "$workdir\acme-certs\$HostName\$HostName-crt.pem" -PathType Leaf) {
        Write-Host -Object "Certificates already exist."
        $timenow = $(Get-Date)
        $certtime = (Get-ChildItem -Path "$workdir\acme-certs\$HostName\$HostName-crt.pem").LastWriteTime
        $difference = $timenow - $certtime
        if ($difference.Days -ge 30) {
            Write-Host -Object "Certificates are older than 30 days, ordering certificates from ACME server..."
            # call winacme client from container and create acme cert files
            Invoke-ScriptInBcContainer -containerName $ContName -scriptblock {
                Param($workdir, $rootpath, $HostName, $adminemail)
                &$workdir\win-acme\wacs.exe `
                    --source manual --host $HostName `
                    --validationmode "http-01" --validation filesystem --webroot $rootpath --manualtargetisiis `
                    --order host `
                    --csr rsa `
                    --store pemfiles --pemfilespath "$workdir\acme-certs\$HostName" `
                    --installation none `
                    --setuptaskscheduler `
                    --accepttos `
                    --emailaddress $adminemail `
                    --verbose
            } -argumentList $workdir, $rootpath, $HostName, $adminemail
        }
        else {
            Write-Host -Object "Certificates are only $($difference.Days) older, update is not needed."
        }
    }
    else {
        Write-Host -Object "Certificates don't exist, ordering certificates from ACME server..."
        # call winacme client from container and create acme cert files
        Invoke-ScriptInBcContainer -containerName $ContName -scriptblock {
            Param($workdir, $rootpath, $HostName, $adminemail)
            &$workdir\win-acme\wacs.exe `
                --source manual --host $HostName `
                --validationmode "http-01" --validation filesystem --webroot $rootpath --manualtargetisiis `
                --order host `
                --csr rsa `
                --store pemfiles --pemfilespath "$workdir\acme-certs\$HostName" `
                --installation none `
                --setuptaskscheduler `
                --accepttos `
                --emailaddress $adminemail `
                --verbose
        } -argumentList $workdir, $rootpath, $HostName, $adminemail
    }

    # stop ngrok tunnel to solve acme challenge
    Write-Host -Object "Stopping ngrok-acme service..."
    Stop-Service -Name "ngrok-acme" -Force

    # start previously running ngrok services
    Write-Host -Object "Restarting ngrok services..."
    $services | ForEach-Object {
        $service = $_
        if ($service.Status -eq "Running") {
            if ((Get-Service -Name $service.Name).Status -ne "Running") {
                Start-Service -Name $service.Name -Verbose
            }
        }
    }

}


function global:Install-NSSM {

    param (

        [Parameter(Mandatory = $False)]
        [switch] $Update

    )

    $uri = "https://nssm.cc/download/"
    $workdir = "C:\ProgramData\Ngrok-WinACME"

    # determine latest release
    $web = Invoke-WebRequest -Uri $uri -UseBasicParsing -Method Get -MaximumRedirection 0
    $latestver = ($web.Links.href | Select-String -SimpleMatch -Pattern "release").ToString().Replace("/release/nssm-", "").Replace(".zip", "")

    # determine the download link
    $link = "https://nssm.cc" + ($web.Links.href | Select-String -SimpleMatch -Pattern "release").ToString()
    $dirname = $link.Replace("https://nssm.cc/release/", "").Replace(".zip", "")

    if ($Update) {
        if (Test-Path -Path "$workdir\nssm\win64\nssm.exe" -PathType Leaf) {
        }
        else {
            Write-Host -Object "NSSM installation not found."
            return
        }
        Write-Host -Object "Checking installed and latest available version..."
        $currentver = ((&$workdir\nssm\win64\nssm.exe *>&1).Exception.Message | Select-String -SimpleMatch -Pattern "V" | Select-Object -First 1 -Skip 1).ToString().Split(' ') | Select-Object -First 1 -Skip 1
        Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
        Write-Host -ForegroundColor Yellow -Object "Available version: $latestver"
        if ($currentver -eq $latestver) {
            Write-Host -Object "Latest version already installed."
        }
        else {
            Write-Host -Object "Downloading the latest installer..."
            try {
                Invoke-WebRequest -Uri $link -Method Get -OutFile "$workdir\nssm.zip"
            }
            catch {
                Write-Warning -Message "Download failed!"
                return
            }
            Write-Host -Object "Removing the existing installation..."
            Remove-Item -Path "$workdir\nssm" -Recurse -Force
            Write-Host -Object "Installing the downloaded version..."
            Expand-Archive -Path "$workdir\nssm.zip" -DestinationPath "$workdir" -Force
            Rename-Item -Path $workdir\$dirname -NewName "nssm" -Force
            $currentver = ((&$workdir\nssm\win64\nssm.exe *>&1).Exception.Message | Select-String -SimpleMatch -Pattern "V" | Select-Object -First 1 -Skip 1).ToString().Split(' ') | Select-Object -First 1 -Skip 1
            Write-Host -Object "Removing the downloaded installer..."
            Remove-Item -Path "$workdir\nssm.zip" -Force
            Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
        }
        return
    }

    if (Test-Path -Path "$workdir\nssm\win64\nssm.exe" -PathType Leaf) {
        Write-Host -Object "NSSM already installed, checking installed and latest available version..."
        $currentver = ((&$workdir\nssm\win64\nssm.exe *>&1).Exception.Message | Select-String -SimpleMatch -Pattern "V" | Select-Object -First 1 -Skip 1).ToString().Split(' ') | Select-Object -First 1 -Skip 1
        Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
        Write-Host -ForegroundColor Yellow -Object "Available version: $latestver"
    }
    else {
        Write-Host -Object "NSSM installation not found!"
        Write-Host -Object "Downloading the installer..."
        try {
            Invoke-WebRequest -Uri $link -Method Get -OutFile "$workdir\nssm.zip"
        }
        catch {
            Write-Warning -Message "Download failed!"
            return
        }
        Write-Host -Object "Installing..."
        Expand-Archive -Path "$workdir\nssm.zip" -DestinationPath "$workdir" -Force
        Rename-Item -Path $workdir\$dirname -NewName "nssm" -Force
        $currentver = ((&$workdir\nssm\win64\nssm.exe *>&1).Exception.Message | Select-String -SimpleMatch -Pattern "V" | Select-Object -First 1 -Skip 1).ToString().Split(' ') | Select-Object -First 1 -Skip 1
        Write-Host -Object "Removing the downloaded installer..."
        Remove-Item -Path "$workdir\nssm.zip" -Force
        Write-Host -ForegroundColor Yellow -Object "Installed version: $currentver"
    }

}


# this function handles the help display
function global:Show-Help {

    Write-Host -NoNewline -Object "`n"

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Install-Ngrok`t`t`t"
    Write-Host -Object "Installs ngrok client."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -Force`t`t`t"
    Write-Host -Object "Force the install/upgrade over existing installation."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Install-NSSM`t`t`t"
    Write-Host -Object "Installs NSSM service helper."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -Update`t`t`t"
    Write-Host -Object "Downloads the latest update if available."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Install-WinACME`t`t`t"
    Write-Host -Object "Installs Win-ACME client."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -Update`t`t`t"
    Write-Host -Object "Downloads the latest update if available."

    Write-Host -NoNewline -Object "`n"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "Ngrok-AuthToken`t`t`t"
    Write-Host -Object "Updates authtoken in ngrok config files."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -AuthToken"
    Write-Host -NoNewline -Object " ABC`t`t"
    Write-Host -Object "Specify value for ngrok authotken."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -ConfigFile"
    Write-Host -NoNewline -Object " config.yml`t"
    Write-Host -Object "Specify name of the yml config file."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Ngrok-InstallService`t`t"
    Write-Host -NoNewline -Object "Installs ngrok background service."
    Write-Host -ForegroundColor Yellow -Object " (RunAsAdmin)"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -ConfigFile"
    Write-Host -NoNewline -Object " config.yml`t"
    Write-Host -Object "Specify name of the yml config file."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Ngrok-RemoveService`t`t"
    Write-Host -NoNewline -Object "Removes the ngrok background service."
    Write-Host -ForegroundColor Yellow -Object " (RunAsAdmin)"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -ConfigFile"
    Write-Host -NoNewline -Object " config.yml`t"
    Write-Host -Object "Specify name of the yml config file."
    Write-Host -NoNewline -Object "`n"

    Write-Host -NoNewline -ForegroundColor Yellow -Object "WinACME-NewNgrokCert`t`t"
    Write-Host -NoNewline -Object "Issue a new ACME LetsEncrypt CA certificate for ngrok tunnel connected to BC container. Order will not be placed if certificates already exist and are not older than 30 days."
    Write-Host -ForegroundColor Yellow -Object " (RunAsAdmin)"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -ContName"
    Write-Host -NoNewline -Object " demo`t`t"
    Write-Host -Object "Specify BC container name."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -HostName"
    Write-Host -NoNewline -Object " FQDN`t`t"
    Write-Host -NoNewline -Object "Specify hostname."
    Write-Host -ForegroundColor Yellow -Object " (e.g. abc.xyz.com)"

    Write-Host -NoNewline -ForegroundColor Yellow -Object "WinACME-NewWinRMCert`t`t"
    Write-Host -NoNewline -Object "Issue a new ACME LetsEncrypt CA certificate for WinRM server. New order is placed only if existing certificate is about to expire."
    Write-Host -ForegroundColor Yellow -Object " (RunAsAdmin)"

    Write-Host -NoNewline -Object "`n"

}

# show help on first run
Show-Help
