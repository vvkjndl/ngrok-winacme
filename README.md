# Ngrok WinACME

- IIS must be installed and Default Web Site must be configured with correct public hostname.
- Clone this repository on desktop and import the script as module.

`
Import-Module -FullyQualifiedName "$home\Desktop\ngrok-winacme\Ngrok-ACME.ps1" -DisableNameChecking -Force
`


## Ngrok-ACME.ps1

- This script handles the installation and upgrade of software Ngrok, WinACME and NSSM.
- This script helps to put ngrok process as a background service using NSSM.
- This script can be used to update authtokens for ngrok config files.
- This script helps you to generate LetsEncrypt CA certificates for BC Docker Containers to be used by ngrok TLS tunnels. ACME challanges are solved using win-acme.
- This script helps you to generate LetsEncrypt CA certificates for WinRM server. ACME challanges are solved using win-acme.

```
Install-Ngrok                   Installs ngrok client.
  -Force                        Force the install/upgrade over existing installation.
Install-NSSM                    Installs NSSM service helper.
  -Update                       Downloads the latest update if available.
Install-WinACME                 Installs Win-ACME client.
  -Update                       Downloads the latest update if available.

Ngrok-AuthToken                 Updates authtoken in ngrok config files.
  -AuthToken ABC                Specify value for ngrok authotken.
  -ConfigFile config.yml        Specify name of the yml config file.
Ngrok-InstallService            Installs ngrok background service. (RunAsAdmin)
  -ConfigFile config.yml        Specify name of the yml config file.
Ngrok-RemoveService             Removes the ngrok background service. (RunAsAdmin)
  -ConfigFile config.yml        Specify name of the yml config file.

WinACME-NewNgrokCert            Issue a new ACME LetsEncrypt CA certificate for ngrok tunnel connected to BC container. Order will not be placed if certificates already exist and are not older than 30 days. (RunAsAdmin)
  -ContName demo                Specify BC container name.
  -HostName FQDN                Specify hostname. (e.g. abc.xyz.com)
WinACME-NewWinRMCert            Issue a new ACME LetsEncrypt CA certificate for WinRM server. New order is placed only if existing certificate is about to expire. (RunAsAdmin)
```


## config.yml

You can define ngrok tunnel definitions here.


## acme.yml

This config file is manipulated by script automatedly to open ngrok http tunnel to solve ACME challanges.
